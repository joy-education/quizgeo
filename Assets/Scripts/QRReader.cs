﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using ZXing;
using ZXing.QrCode;

namespace JoyEducation
{
    [System.Serializable]
    public class QREvent : UnityEvent { }

    public class QRReader : MonoBehaviour
    {
        public string AuthenticationKey;
        public Text Text;
        public RawImage CamImage;

        public QREvent OnAuthentication;
        
        private bool _hasInitialized;

        private WebCamTexture camTexture;
        public bool IsAuthenticated
        {
            get
            {
                return PlayerPrefs.GetInt("Authentication") == 1;
            }
            private set
            {
                PlayerPrefs.SetInt("Authentication", value == true ? 1 : 0);
            }
        }

        void Start()
        {
            Init();            
        }

        private void LateUpdate()
        {
            if (IsAuthenticated)
                return;

            if (!_hasInitialized)
                Init();

            if (WebCamTexture.devices.Length <= 0)
                return;

            try
            {
                IBarcodeReader barcodeReader = new BarcodeReader();

                var result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
                if (result != null)
                {
                    if (result.Text == AuthenticationKey)
                    {
                        Debug.LogWarning("Authenticated!");

                        Text.text = "Autenticado!";
                        IsAuthenticated = true;

                        CoroutineManager.WaitForSeconds(1, () => 
                        {
                            OnAuthentication.Invoke();
                        });
                    }
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogWarning(ex.Message);
            }
        }

        private void Init()
        {
            if (WebCamTexture.devices.Length <= 0 && !Application.isEditor)
                return;

            if (IsAuthenticated || Application.isEditor)
            {
                OnAuthentication.Invoke();
                return;
            }

            camTexture = new WebCamTexture();
            if (camTexture != null)
            {
                camTexture.Play();
                CamImage.texture = camTexture;
                _hasInitialized = true;
            }

        }
    }
}
