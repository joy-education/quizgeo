﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JoyEducation;

public class AnswerButton : MonoBehaviour
{
    private string _answer;

    private Text _text;
    public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();
            return _text;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            GameManager.Instance.Answer(_answer);
        });
    }

    public void SetAnswer(string answer)
    {
        _answer = answer;
        Text.text = answer;
    }
}
