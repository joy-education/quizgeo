﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerFlag : MonoBehaviour
{
    public Text Text;
    public Image Placeholder;

    private Flag _flag;
    public Flag Flag
    {
        get
        {
            return _flag;
        }
        set
        {
            _flag = value;
            Image.sprite = Flag.Sprite;

            if (Text != null && Text.gameObject.activeSelf)
                Text.text = Flag.State;
        }
    }

    private Region _region;
    public Region Region
    {
        get
        {
            return _region;
        }
        set
        {
            _region = value;
            Image.sprite = Region.Sprite;
        }
    }

    private Image _image;
    public Image Image
    {
        get
        {
            if (_image == null)
                _image = GetComponent<Image>();
            return _image;
        }
    }

    public void EnableText(bool enabled)
    {
        if (Text != null)
        {
            Text.gameObject.SetActive(enabled);
            //Image.enabled = !enabled;
            //Placeholder.enabled = enabled;
        }

    }
}
