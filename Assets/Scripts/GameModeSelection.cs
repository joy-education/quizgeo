﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConfigs
{
    public static GameMode GameMode = GameMode.None;
}

public class GameModeSelection : MonoBehaviour
{
    public MainMenuSceneManager MainMenuSceneManager;

    public void SetGameModeStates()
    {
        SetGameMode(GameMode.States);
    }

    public void SetGameModeRegions()
    {
        SetGameMode(GameMode.Regions);
    }

    public void SetGameModeCapitals()
    {
        SetGameMode(GameMode.Capitals);
    }

    public void SetGameMode(GameMode mode)
    {
        GameConfigs.GameMode = mode;
        MainMenuSceneManager.ChangeScene("Gameplay");
    }
}
