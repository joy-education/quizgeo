﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PotaTween;
using JoyEducation;

public class MainMenuSceneManager : MonoBehaviour
{
    public MainMenuAnimations Animations;
    public GameModeSelection GameModeSelection;

    private void Awake()
    {
        SceneManager.sceneLoaded += (s, lsm) => { Fader.SceneFadeIn(); };
    }

    private void Start()
    {
        if (!AudioController.MusicIsPlaying(AudioController.Music.Gameplay))
            CoroutineManager.WaitForSeconds(2.6f, () => AudioController.PlayMusic(AudioController.Music.Gameplay));
    }

    public void PlayButtonClicked()
    {
        GameModeSelection.gameObject.SetActive(true);
    }

    public void ChangeScene(string sceneName)
    {
        Animations.PlayButtonClickedAnimation(() => 
        {
            Fader.SceneFadeOut(() => 
            {
                SceneManager.LoadScene(sceneName);
            });
        });
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
