﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PotaTween;

public class MainMenuAnimations : MonoBehaviour
{
    [System.Serializable]
    public class MenuAnimation
    {
        public GameObject GameObject;
        public string Tag;

        public Tween Tween
        {
            get
            {
                return Tween.Get(GameObject, Tag);
            }
        }
    }

    public GameObject Map;
    public List<MenuAnimation> EntryAnimations;
    public List<MenuAnimation> ExitAnimations;

    public void PlayButtonClickedAnimation(System.Action callback = null)
    {
        Tween.Get(Map, "Play1").Play(() =>
        {
            Tween.Get(Map, "Play2").Play(() =>
            {
                callback?.Invoke();
            });
        });

        PlayExit();
    }

    public void PlayEntry()
    {
        PlayEntry(null);
    }

    public void PlayEntry(System.Action callback)
    {
        PlayAnimations(EntryAnimations, callback);
    }

    public void PlayExit()
    {
        PlayExit(null);
    }

    public void PlayExit(System.Action callback)
    {
        PlayAnimations(ExitAnimations, callback);
    }

    private void PlayAnimations(List<MenuAnimation> anims, System.Action callback)
    {
        float maxAnimLength = 0;
        int longestAnimIndex = 0; ;
        for (int i = 0; i < anims.Count; i++)
        {
            if (anims[i].Tween.Duration > maxAnimLength)
            {
                maxAnimLength = anims[i].Tween.Duration;
                longestAnimIndex = i;
            }
        }

        for (int i = 0; i < anims.Count; i++)
        {
            System.Action cb = null;
            if (i == longestAnimIndex)
                cb = callback;

            anims[i].Tween.Play(cb);
        }
    }
}
