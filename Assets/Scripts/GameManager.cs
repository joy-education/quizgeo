﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using JoyEducation;

public enum GameMode
{
    None,
    States,
    Regions,
    Capitals
}

[System.Serializable]
public class Flag
{
    public string State;
    public string Capital;
    public string Region;
    public string ImagePath;

    [SerializeField]
    private Sprite _sprite;
    public Sprite Sprite
    {
        get
        {
            if (_sprite == null)
                _sprite = Resources.Load<Sprite>(ImagePath);
            return _sprite;
        }
    }
}

[System.Serializable]
public class Region
{
    public string Name;
    public string ImagePath;

    [SerializeField]
    private Sprite _sprite;
    public Sprite Sprite
    {
        get
        {
            if (_sprite == null)
                _sprite = Resources.Load<Sprite>(ImagePath);
            return _sprite;
        }
    }
}

public class GameManager : MonoBehaviour
{
    [Header("UI")]
    public Openable GameArea;
    public TextAnimation PositiveFeedback;
    public TextAnimation NegatibeFeedback;
    public Openable EndGamePopup;
    public ReportScroll ReportScroll;
    public Text QuestionText;
    public AnswerFlag AnswerFlag;
    public List<AnswerButton> AnswerButtons;
    public Text QuestionCountText;

    [Header("Game Configs")]
    public GameMode GameMode = GameMode.States;
    public int NumberOfQuestions = 10;
    public List<Flag> Flags;
    public List<Region> Regions;

    private List<Flag> _flags;
    private List<Region> _regions;
    private string _report;
    private int _rightAnswers;

    private int _questionCount;
    public int QuestionCount
    {
        get
        {
            return _questionCount;
        }
        set
        {
            _questionCount = value;
            QuestionCountText.text = $"{_questionCount.ToString("00")}/{NumberOfQuestions}";
        }
    }

    private static GameManager _instace;
    public static GameManager Instance
    {
        get
        {
            if (_instace == null)
                _instace = FindObjectOfType<GameManager>();
            return _instace;
        }
    }

    private void Awake()
    {
        Fader.SceneFadeIn();

        if (GameConfigs.GameMode != GameMode.None)
            GameMode = GameConfigs.GameMode;

        switch (GameMode)
        {
            case GameMode.None:
                break;
            case GameMode.States:
                _report += "\nModo de Jogo: Bandeiras\n";
                break;
            case GameMode.Regions:
                _report += "\nModo de Jogo: Regiões\n";
                break;
            case GameMode.Capitals:
                _report += "\nModo de Jogo: Capitais\n";
                break;
            default:
                break;
        }
    }

    private void Start()
    {
        _flags = new List<Flag>(Flags);
        _regions = new List<Region>(Regions);
        NewQuestion();

        if (!AudioController.MusicIsPlaying(AudioController.Music.Gameplay))
            AudioController.PlayMusic(AudioController.Music.Gameplay);
    }

    public void Answer(string answer)
    {
        GameArea.SetActive(false);

        _report += $"Resposta dada: {answer}\n";
        
        if (CheckAnswer(answer))
        {
            PositiveFeedback.SetActive(true);
            _report += $"Correto!\n";
            _rightAnswers++;

            AudioController.PlaySFX(AudioController.SFX.RightAnswer);
        }
        else
        {
            NegatibeFeedback.SetActive(true);
            _report += $"Errado...\n";

            AudioController.PlaySFX(AudioController.SFX.WrongAnswer);
        }

        CoroutineManager.WaitForSeconds(2, () => 
        {
            if (PositiveFeedback.IsActive)
                PositiveFeedback.SetActive(false);
            if (NegatibeFeedback.IsActive)
                NegatibeFeedback.SetActive(false);

            if (CheckEndGame())
            {
                _report += $"\nFim de Jogo!\nTotal de respostas certas: {_rightAnswers}\n";
                ReportScroll.ContentText.text = _report;

                ReportScroll.SetActive(true);
                //EndGamePopup.SetActive(true);
            }
            else
            {
                NewQuestion();
            }
        });
    }

    public void ChangeScene(string sceneName)
    {
        if (GameArea.IsActive)
        {
            GameArea.SetActive(false, () => 
            {
                Fader.SceneFadeOut(() => 
                {
                    SceneManager.LoadScene(sceneName);
                });
            });
        }
        else if(EndGamePopup.IsActive)
        {
            EndGamePopup.SetActive(false, () =>
            {
                Fader.SceneFadeOut(() => 
                {
                    SceneManager.LoadScene(sceneName);
                });
            });
        }
        else
            Fader.SceneFadeOut(() =>
            {
                SceneManager.LoadScene(sceneName);
            });
    }

    private void NewQuestion()
    {        
        GameArea.SetActive(true);
        QuestionCount++;
       
        switch (GameMode)
        {
            case GameMode.None:
                break;
            case GameMode.States:
                NewQuestionState();
                break;
            case GameMode.Regions:
                NewQuestionRegion();
                break;
            case GameMode.Capitals:
                NewQuestionCapital();
                break;
            default:
                break;
        }
    }

    private void NewQuestionState()
    {       
        QuestionText.text = "A qual estado pertence a seguinte bandeira?";
        AnswerFlag.EnableText(false);

        int flagIndex = Random.Range(0, _flags.Count);
        Flag answer = _flags[flagIndex];
        _flags.Remove(answer);

        AnswerFlag.Flag = answer;

        List<Flag> flags = new List<Flag>(Flags);
        flags.Remove(answer);

        int correctButtonIndex = Random.Range(0, AnswerButtons.Count);
        for (int i = 0; i < AnswerButtons.Count; i++)
        {
            if (i == correctButtonIndex)
            {
                AnswerButtons[i].SetAnswer(answer.State);
            }
            else
            {
                int wrongFlagIndex = Random.Range(0, flags.Count);
                Flag wrongFlag = flags[wrongFlagIndex];
                flags.Remove(wrongFlag);
                AnswerButtons[i].SetAnswer(wrongFlag.State);
            }
        }

        _report += $"\nResposta certa: {answer.State}\n";
    }

    private void NewQuestionRegion()
    {
        QuestionText.text = "Qual estado é pertencente à seguinte região?";
        AnswerFlag.EnableText(false);

        int regionIndex = Random.Range(0, _regions.Count);
        Region answer = _regions[regionIndex];
        //_regions.Remove(answer);

        AnswerFlag.Region = answer;

        List<Flag> flags = new List<Flag>(Flags);

        List<Flag> answerFlags = new List<Flag>();
        foreach (var flag in _flags)
        {
            if (flag.Region == answer.Name)
            {
                answerFlags.Add(flag);
            }
        }

        if (answerFlags.Count <= 0)
        {
            _regions.Remove(answer);
            NewQuestionRegion();
            return;
        }

        foreach (var flag in answerFlags)
        {
            flags.Remove(flag);
        }

        int answerFlagIndex = Random.Range(0, answerFlags.Count);
        Flag answerFlag = answerFlags[answerFlagIndex];
        _flags.Remove(answerFlag);

        int correctButtonIndex = Random.Range(0, AnswerButtons.Count);
        for (int i = 0; i < AnswerButtons.Count; i++)
        {
            if (i == correctButtonIndex)
            {
                AnswerButtons[i].SetAnswer(answerFlag.State);
            }
            else
            {
                int wrongFlagIndex = Random.Range(0, flags.Count);
                Flag wrongFlag = flags[wrongFlagIndex];
                flags.Remove(wrongFlag);
                AnswerButtons[i].SetAnswer(wrongFlag.State);
            }
        }

        _report += $"\nResposta certa: {answerFlag.State}\n";
    }

    private void NewQuestionCapital()
    {
        QuestionText.text = "Qual a capital do seguinte estado?";
        AnswerFlag.EnableText(true);

        int flagIndex = Random.Range(0, _flags.Count);
        Flag answer = _flags[flagIndex];
        _flags.Remove(answer);

        AnswerFlag.Flag = answer;

        List<Flag> flags = new List<Flag>(Flags);
        flags.Remove(answer);

        int correctButtonIndex = Random.Range(0, AnswerButtons.Count);
        for (int i = 0; i < AnswerButtons.Count; i++)
        {
            if (i == correctButtonIndex)
            {
                AnswerButtons[i].SetAnswer(answer.Capital);
            }
            else
            {
                int wrongFlagIndex = Random.Range(0, flags.Count);
                Flag wrongFlag = flags[wrongFlagIndex];
                flags.Remove(wrongFlag);
                AnswerButtons[i].SetAnswer(wrongFlag.Capital);
            }
        }

        _report += $"\nResposta certa: {answer.Capital}\n";
    }

    private bool CheckAnswer(string answer)
    {
        //print(answer);

        switch (GameMode)
        {
            default:
            case GameMode.None:
                return false;
            case GameMode.States:
                return answer == AnswerFlag.Flag.State;
            case GameMode.Regions:
                foreach (var state in Flags)
                {
                    if (state.State == answer)
                    {
                        if (state.Region == AnswerFlag.Region.Name)
                            return true;
                        else
                            return false;
                    }
                    else
                        continue;
                }
                return false;
            case GameMode.Capitals:
                return answer == AnswerFlag.Flag.Capital;
        }
        
    }

    private bool CheckEndGame()
    {
        return QuestionCount >= NumberOfQuestions;
    }
}
