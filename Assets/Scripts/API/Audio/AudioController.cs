﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace JoyEducation
{
    [System.Serializable]
    public class GameAudio
    {
        public string Name;
        public AudioClip Clip;
    }

    [System.Serializable]
    public class SFX
    {
        public AudioClip Clip;
        private AudioSource _audioSource;
        public AudioSource AudioSource
        {
            get
            {
                if (_audioSource == null)
                {
                    _audioSource = new GameObject($"{Clip.name} Audio").AddComponent<AudioSource>();
                    _audioSource.clip = Clip;
                    _audioSource.spatialBlend = 0;
                    _audioSource.playOnAwake = false;
                    _audioSource.transform.SetParent(AudioManager.Instance.transform);
                    _audioSource.outputAudioMixerGroup = AudioController.Instance.AudioMixer.FindMatchingGroups("SFX")[0];
                }

                return _audioSource;
            }
        }

        public SFX(AudioClip clip)
        {
            Clip = clip;
        }
    }

    public class AudioManager : MonoBehaviour
    {
        private static AudioManager _instance;
        public static AudioManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<AudioManager>();
                if (_instance == null)
                    _instance = new GameObject("AudioManager").AddComponent<AudioManager>();
                return _instance;
            }
        }

        private void Awake()
        {
            if (Instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }
    }

    [CreateAssetMenu]
    public class AudioController : ScriptableObject
    {
        public AudioMixer AudioMixer;

        [SerializeField]
        private SFXList _sfx;
        [SerializeField]
        private MusicList _music;
        [SerializeField]
        private VoiceList _voice;

        public static SFXList SFX
        {
            get
            {
                return AudioController.Instance._sfx;
            }
        }

        public static MusicList Music
        {
            get
            {
                return AudioController.Instance._music;
            }
        }

        public static VoiceList Voice
        {
            get
            {
                return AudioController.Instance._voice;
            }
        }

        private List<SFX> _sfxs = new List<SFX>();

        private static AudioSource _musicAudioSource;
        public static AudioSource MusicAudioSource
        {
            get
            {
                if (_musicAudioSource == null)
                {
                    _musicAudioSource = new GameObject("Music Audio").AddComponent<AudioSource>();
                    _musicAudioSource.spatialBlend = 0;
                    _musicAudioSource.playOnAwake = false;
                    _musicAudioSource.loop = true;
                    _musicAudioSource.transform.SetParent(AudioManager.Instance.transform);
                    _musicAudioSource.outputAudioMixerGroup = Instance.AudioMixer.FindMatchingGroups("Music")[0];
                }

                return _musicAudioSource;
            }
        }

        private static AudioSource _voiceAudioSource;
        public static AudioSource VoiceAudioSource
        {
            get
            {
                if (_voiceAudioSource == null)
                {
                    _voiceAudioSource = new GameObject("Voice Audio").AddComponent<AudioSource>();
                    _voiceAudioSource.spatialBlend = 0;
                    _voiceAudioSource.playOnAwake = false;
                    _voiceAudioSource.loop = false;
                    _voiceAudioSource.transform.SetParent(AudioManager.Instance.transform);
                    _voiceAudioSource.outputAudioMixerGroup = Instance.AudioMixer.FindMatchingGroups("Voice")[0];
                }

                return _voiceAudioSource;
            }
        }

        private static AudioController _instance;
        public static AudioController Instance
        {
            get
            {
                return _instance;
            }
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
            AudioController[] controllers = Resources.LoadAll<AudioController>("");
            if (controllers == null || controllers.Length <= 0)
                return;
            _instance = controllers[0];
        }

        public static void PlaySFX(AudioClip audio, System.Action callback = null)
        {

            SFX sfx = Instance._sfxs.Find((s) => s.Clip == audio && !s.AudioSource.isPlaying);
            if (sfx == null)
            {
                sfx = new SFX(audio);
                Instance._sfxs.Add(sfx);
            }
            sfx.AudioSource.Stop();
            sfx.AudioSource.Play();
        }

        public static void PlayMusic(AudioClip music)
        {
            MusicAudioSource.Stop();
            MusicAudioSource.clip = music;
            MusicAudioSource.Play();
        }

        public static void StopMusic()
        {
            MusicAudioSource.Stop();
        }

        public static bool MusicIsPlaying(AudioClip music)
        {
            return MusicAudioSource.clip == music && MusicAudioSource.isPlaying;
        }

        public static void PlayVoice(AudioClip voice, System.Action callback = null)
        {
            VoiceAudioSource.Stop();
            VoiceAudioSource.clip = voice;
            VoiceAudioSource.Play();
            if (callback != null)
                CoroutineManager.WaitForSeconds(VoiceAudioSource.clip.length, callback);
        }

        public static void PlayVoice(AudioClip voice, float delay, System.Action callback = null)
        {
            CoroutineManager.WaitForSeconds(delay, () =>
            {
                PlayVoice(voice, callback);
            });
        }

        public static void PlayVoice(string name, System.Action callback = null)
        {
            AudioClip voice = Voice.GetFromTag(name);            

            PlayVoice(voice, callback);
        }
    }
}