﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JoyEducation
{
    public class AudioHelper : MonoBehaviour
    {
        public void PlaySFX(AudioClip clip)
        {
            AudioController.PlaySFX(clip);
        }
    }
}
