﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace JoyEducation
{
    [System.Serializable]
    public class LocalizedAudio
    {
        public string LocalizationTag;
        public AudioClip AudioClip;
    }

    [CreateAssetMenu]
    public class Voice : ScriptableObject
    {
        public string Tag;
        public List<LocalizedAudio> LocalizedAudios;

        public AudioClip CurrentLocalization
        {
            get
            {
                string localizationTag = "PTBR";
                AudioClip clip = null;

                foreach (var audio in LocalizedAudios)
                {
                    if (audio.LocalizationTag == localizationTag)
                    {
                        clip = audio.AudioClip;
                        break;
                    }
                }

                return clip;
            }
        }
    }
}
