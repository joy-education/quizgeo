﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JoyEducation
{
    public class SoundLists
    {

    }

    [System.Serializable]
    public class SFXList
    {
        public AudioClip ButtonPressed;
        public AudioClip RightAnswer;
        public AudioClip WrongAnswer;
        public AudioClip Congratulations;
    }

    [System.Serializable]
    public class MusicList
    {
        public AudioClip Gameplay;
    }

    [System.Serializable]
    public class VoiceList
    {
        public LocalizedVoiceList PTBR;

        public LocalizedVoiceList CurrentLocalization
        {
            get
            {
                return PTBR;
            }
        }

        public AudioClip GetFromTag(string tag)
        {
            AudioClip voice = null;
            switch (tag.ToLower())
            {
                default:
                    break;

                case "zero":
                case "0":
                    voice = CurrentLocalization.Zero;
                    break;

                case "one":
                case "1":
                    voice = CurrentLocalization.One;
                    break;

                case "two":
                case "2":
                    voice = CurrentLocalization.Two;
                    break;

                case "three":
                case "3":
                    voice = CurrentLocalization.Three;
                    break;

                case "four":
                case "4":
                    voice = CurrentLocalization.Four;
                    break;

                case "five":
                case "5":
                    voice = CurrentLocalization.Five;
                    break;

                case "six":
                case "6":
                    voice = CurrentLocalization.Six;
                    break;

                case "seven":
                case "7":
                    voice = CurrentLocalization.Seven;
                    break;

                case "eight":
                case "8":
                    voice = CurrentLocalization.Eight;
                    break;

                case "nine":
                case "9":
                    voice = CurrentLocalization.Nine;
                    break;

                case "ten":
                case "10":
                    voice = CurrentLocalization.Ten;
                    break;

                case "a":
                    voice = CurrentLocalization.A;
                    break;

                case "b":
                    voice = CurrentLocalization.B;
                    break;

                case "c":
                    voice = CurrentLocalization.C;
                    break;

                case "d":
                    voice = CurrentLocalization.D;
                    break;

                case "e":
                    voice = CurrentLocalization.E;
                    break;

                case "f":
                    voice = CurrentLocalization.F;
                    break;

                case "g":
                    voice = CurrentLocalization.G;
                    break;

                case "h":
                    voice = CurrentLocalization.H;
                    break;

                case "i":
                    voice = CurrentLocalization.I;
                    break;

                case "j":
                    voice = CurrentLocalization.J;
                    break;

                case "k":
                    voice = CurrentLocalization.K;
                    break;

                case "l":
                    voice = CurrentLocalization.L;
                    break;

                case "m":
                    voice = CurrentLocalization.M;
                    break;

                case "n":
                    voice = CurrentLocalization.N;
                    break;

                case "o":
                    voice = CurrentLocalization.O;
                    break;

                case "p":
                    voice = CurrentLocalization.P;
                    break;

                case "q":
                    voice = CurrentLocalization.Q;
                    break;

                case "r":
                    voice = CurrentLocalization.R;
                    break;

                case "s":
                    voice = CurrentLocalization.S;
                    break;

                case "t":
                    voice = CurrentLocalization.T;
                    break;

                case "u":
                    voice = CurrentLocalization.U;
                    break;

                case "v":
                    voice = CurrentLocalization.V;
                    break;

                case "w":
                    voice = CurrentLocalization.W;
                    break;

                case "x":
                    voice = CurrentLocalization.X;
                    break;

                case "y":
                    voice = CurrentLocalization.Y;
                    break;

                case "z":
                    voice = CurrentLocalization.Z;
                    break;

                case "á":
                    voice = CurrentLocalization.AAgudo;
                    break;

                case "â":
                    voice = CurrentLocalization.ACircunflexo;
                    break;

                case "ã":
                    voice = CurrentLocalization.ATil;
                    break;

                case "ç":
                    voice = CurrentLocalization.Cedilha;
                    break;

                case "é":
                    voice = CurrentLocalization.EAgudo;
                    break;

                case "ê":
                    voice = CurrentLocalization.ECircunflexo;
                    break;

                case "í":
                    voice = CurrentLocalization.IAgudo;
                    break;

                case "ó":
                    voice = CurrentLocalization.OAgudo;
                    break;

                case "ô":
                    voice = CurrentLocalization.OCircunflexo;
                    break;

                case "õ":
                    voice = CurrentLocalization.OTil;
                    break;

                case "ú":
                    voice = CurrentLocalization.UAgudo;
                    break;
            }

            return voice;
        }
    }

    [System.Serializable]
    public class LocalizedVoiceList
    {
        public AudioClip Addition;
        public AudioClip Back;
        public AudioClip CloseApp;
        public AudioClip Config;
        public AudioClip ConfigSound;
        public AudioClip Confirm;
        public AudioClip Congratz;
        public AudioClip Consonants;
        public AudioClip Continue;
        public AudioClip Correct;
        public AudioClip Credits;
        public AudioClip Dictation;
        public AudioClip LetsPlay;
        public AudioClip AlphabetLetters;
        public AudioClip Multiplication;
        public AudioClip Numbers;
        public AudioClip Painting;
        public AudioClip Play;
        public AudioClip PlayAgain;
        public AudioClip Subtraction;
        public AudioClip Title;
        public AudioClip ToMainMenu;
        public AudioClip TryAgain;
        public AudioClip Vowels;
        public AudioClip Welcome;
        public AudioClip Words;
        public AudioClip Wrong;
        public AudioClip Zero;
        public AudioClip One;
        public AudioClip Two;
        public AudioClip Three;
        public AudioClip Four;
        public AudioClip Five;
        public AudioClip Six;
        public AudioClip Seven;
        public AudioClip Eight;
        public AudioClip Nine;
        public AudioClip Ten;
        public AudioClip A;
        public AudioClip B;
        public AudioClip C;
        public AudioClip D;
        public AudioClip E;
        public AudioClip F;
        public AudioClip G;
        public AudioClip H;
        public AudioClip I;
        public AudioClip J;
        public AudioClip K;
        public AudioClip L;
        public AudioClip M;
        public AudioClip N;
        public AudioClip O;
        public AudioClip P;
        public AudioClip Q;
        public AudioClip R;
        public AudioClip S;
        public AudioClip T;
        public AudioClip U;
        public AudioClip V;
        public AudioClip W;
        public AudioClip X;
        public AudioClip Y;
        public AudioClip Z;
        public AudioClip AAgudo;
        public AudioClip ACircunflexo;
        public AudioClip ACrase;
        public AudioClip ATil;
        public AudioClip CCedilha;
        public AudioClip Cedilha;
        public AudioClip EAgudo;
        public AudioClip ECircunflexo;
        public AudioClip ECrase;
        public AudioClip IAgudo;
        public AudioClip ICrase;
        public AudioClip OAgudo;
        public AudioClip OCircunflexo;
        public AudioClip OCrase;
        public AudioClip OTil;
        public AudioClip UAgudo;
        public AudioClip UCircunflexo;
        public AudioClip UCrase;
    }
}

