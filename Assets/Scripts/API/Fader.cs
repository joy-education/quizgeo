﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PotaTween;

namespace JoyEducation
{
    public class Fader : Openable
    {
        private static Fader _instance;
        public static Fader Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<Fader>();
                if (_instance == null)
                    _instance = CreateFader();

                return _instance;
            }
        }

        public static void SceneFadeIn(System.Action callback = null)
        {
            Instance.SetActive(false, callback);
            Instance.transform.SetAsLastSibling();
        }

        public static void SceneFadeOut(System.Action callback = null)
        {
            Instance.SetActive(true, callback);
            Instance.transform.SetAsLastSibling();
        }

        private static Fader CreateFader()
        {
            Canvas canvas = FindObjectOfType<Canvas>();

            DefaultControls.Resources uiResources = new DefaultControls.Resources();
            uiResources.background = Sprite.Create(null, new Rect(0, 0, 512, 512), new Vector2(0.5f, 0.5f));
            GameObject faderObj = DefaultControls.CreatePanel(uiResources);

            faderObj.name = "Fader";
            Fader fader = faderObj.AddComponent<Fader>();

            Image faderImg = faderObj.GetComponent<Image>();
            faderImg.color = Color.black;

            RectTransform faderTr = faderObj.GetComponent<RectTransform>();
            faderTr.SetParent(canvas.transform, false);
            faderTr.SetAsLastSibling();

            return fader;
        }
    }
}
