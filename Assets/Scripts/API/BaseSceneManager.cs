﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JoyEducation
{
    public class BaseSceneManager : MonoBehaviour
    {
        private void Awake()
        {
            Fader.SceneFadeIn();
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void ChangeScene(string sceneName)
        {
            ChangeScene(sceneName, true);
        }

        public void ChangeScene(string sceneName, bool useFader)
        {
            System.Action loadScene = () => { SceneManager.LoadScene(sceneName); };

            if (useFader)
                Fader.SceneFadeOut(loadScene);
            else
                loadScene();
        }
    }
}
