﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PotaTween;

namespace JoyEducation
{
    public class AlertPopup : MonoBehaviour {

        public Text Text;
        public List<AlertPopupButtonLayout> ButtonLayouts;

        private int _layoutIndex;
        public  int LayoutIndex
        {
            get
            {
                return _layoutIndex;
            }
            set
            {
                _layoutIndex = value;

                for (int i = 0; i < ButtonLayouts.Count; i++)
                {
                    ButtonLayouts[i].gameObject.SetActive(i == _layoutIndex);
                }
            }
        }

        public List<Button> Buttons
        {
            get
            {
                return ButtonLayouts[LayoutIndex].Buttons;
            }
        }

        private Tween _tween;
        public Tween Tween
        {
		    get 
		    {
			    if (_tween == null)
				    _tween = GetComponent<Tween>();

			    return _tween;
		    }
        }

        public void SetActive(bool active, int layoutIndex = 0)
        {
            Tween.Stop();        

            if (active)
            {
                gameObject.SetActive(true);
                LayoutIndex = layoutIndex;
                Tween.Play();
            }
            else
            {
                Tween.Reverse(() => 
                {
                    gameObject.SetActive(false);
                });
            }
        }
    }
}
