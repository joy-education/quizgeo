﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ZXing;
using ZXing.Common;

public class GenerateQRCodeWindow : EditorWindow
{
    private string _data;
    private Vector2 _resolution = new Vector2(256, 256);

    [MenuItem("Component/QRCode")]
    static void Init()
    {
        GenerateQRCodeWindow window = (GenerateQRCodeWindow)EditorWindow.GetWindow(typeof(GenerateQRCodeWindow));
        window.Show();
    }

    private void OnGUI()
    {
        GUILayout.Label("QR Code Generator", EditorStyles.boldLabel);
        _data = EditorGUILayout.TextField("QR Code Data", _data);
        _resolution = EditorGUILayout.Vector2Field("Resolution", _resolution);
        //_resolution = new Vector2(256, 256);

        if (GUILayout.Button("Generate"))
        {
            Texture2D texture = GenerateQRCode(_data, BarcodeFormat.QR_CODE, Mathf.FloorToInt(_resolution.x), Mathf.FloorToInt(_resolution.y));
            byte[] bytes = texture.EncodeToPNG();
            string filename = $"{Application.dataPath}/{_data}_qrcode.png";
            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.LogWarning($"Genreated QR Code saved in {filename}");
            Application.OpenURL(filename);
        }
    }

    /*private static Texture2D GenerateQRCode(string data, Vector2 resolution)
    {
        var encoded = new Texture2D(Mathf.FloorToInt(resolution.x), Mathf.FloorToInt(resolution.y));
        var color32 = Encode(data, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new ZXing.Common.EncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }*/

    private static Texture2D GenerateQRCode(string data, BarcodeFormat format, int width, int height)
    {
        // Generate the BitMatrix
        BitMatrix bitMatrix = new MultiFormatWriter()
            .encode(data, format, width, height);
        // Generate the pixel array
        Color[] pixels = new Color[bitMatrix.Width * bitMatrix.Height];
        int pos = 0;
        for (int y = 0; y < bitMatrix.Height; y++)
        {
            for (int x = 0; x < bitMatrix.Width; x++)
            {
                pixels[pos++] = bitMatrix[x, y] ? Color.black : Color.white;
            }
        }
        // Setup the texture
        Texture2D tex = new Texture2D(bitMatrix.Width, bitMatrix.Height);
        tex.SetPixels(pixels);
        tex.Apply();
        return tex;
    }
}
